import os
import subprocess
import typing as t

from rich.console import RenderableType

from .console import console


class MenuLine(t.Protocol):
    @property
    def menu_line(self) -> RenderableType:
        ...

    @property
    def menu_description(self) -> t.Optional[RenderableType]:
        ...


T_Choice = t.TypeVar("T_Choice", bound=MenuLine)


def fzf(
    choices: t.Sequence[T_Choice],
    prompt: t.Optional[str] = None,
    header: t.Optional[str] = None,
) -> t.Optional[T_Choice]:
    preview_env = {}
    choice_strings = []
    for i, choice in enumerate(choices):
        num = f"{i:04x}"

        with console.capture() as capture:
            console.print(choice.menu_line, highlight=False)
        choice_string = f"{num}!{capture.get()}"

        if choice.menu_description is not None:
            with console.capture() as capture:
                console.print(choice.menu_description, highlight=False, soft_wrap=True)
            preview_env[f"_fzf_desc_{num}"] = capture.get()

        choice_strings.append(choice_string)

    args = [
        "fzf",
        "--reverse",
        "--border",
        "--ansi",
        "--read0",
        "--with-nth=2..",
        "--delimiter=!",
    ]
    if prompt is not None:
        args.append(f"--prompt={prompt}> ")
    if header is not None:
        args.append(f"--header={header}")

    env: t.Optional[dict[str, str]] = None
    if preview_env:
        args.append(
            "--preview=eval \"$(printf 'printf '\\''%%s\\\\n'\\'' \"${_fzf_desc_%s}\"' {1})\""
        )
        args.append("--preview-window=bottom:wrap")
        env = {}
        env.update(os.environ)
        env.update(preview_env)

    cp = subprocess.run(
        args,
        input=b"\0".join(cs.encode("utf-8") for cs in choice_strings),
        stdout=subprocess.PIPE,
        env=env,
    )
    if cp.returncode in (130, 1):
        # no match or interrupted
        return None
    cp.check_returncode()

    res_str = cp.stdout.decode("utf-8").strip()
    if res_str is None:
        return None

    res_num = int(res_str.split("!", 1)[0], 16)
    res = choices[res_num]
    return res
