import os
import sys

from .console import console
from .run import run

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


def upgrade() -> None:
    with console.status("Updating code"):
        run("git", "pull", "--no-rebase", cwd=PROJECT_DIR)
    with console.status("Updating dependencies"):
        if sys.prefix == sys.base_prefix:
            # not in venv/virtualenv
            run(
                "pip",
                "install",
                "--user",
                "-r",
                "requirements.txt",
                cwd=PROJECT_DIR,
            )
        else:
            # inside venv
            run(
                os.path.join(sys.exec_prefix, "bin/pip"),
                "install",
                "-r",
                "requirements.txt",
                cwd=PROJECT_DIR,
            )
        os.sync()
    os.execlp(sys.executable, sys.executable, "-m", __package__, *sys.argv[1:])
