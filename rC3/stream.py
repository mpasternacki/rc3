import typing as t
from collections import defaultdict
from datetime import datetime

from rich.console import RenderableType

from .fahrplan import Entry, Fahrplan
from .menu import Menu
from .mpv import mpv
from .util import TZ, format_duration, md

_T_Stream = t.TypeVar("_T_Stream", bound="Stream")


class Stream:
    name: str
    slug: str
    has_fahrplan: bool

    _fahrplan: t.ClassVar[t.Optional[Fahrplan]] = None

    SLUGS = {
        # "c-base": "cbase",
        "Chaos-West TV": "cwtv",
        "r3s - Monheim/Rhein": "r3s",
        "Chaosstudio Hamburg": "csh",
        "ChaosZone TV": "chaoszone",
        "FeM Channel": "fem",
        # "franconian.net": "franconiannet",
        "about:future stage": "aboutfuture",
        "Sendezentrum Bühne": "sendezentrum",
        "Haecksen Stream": "haecksen",
        "Bierscheune": "gehacktes",
        "xHain Lichtung": "xhain",
        "Lichtung": "xhain",
        "rC3 Lounge": "c3lounge",
        # "Abschillgleis"
    }

    NO_FAHRPLAN = ("Infobeamer",)

    CATEGORIES: dict[str, str] = defaultdict(
        lambda: ":mage:",
        **{
            "Abchillgleis": ":notes:",
            "rC3 Lounge": ":notes:",
            "Infobeamer": ":memo:",
        },
    )

    def __init__(self, name: str):
        self.name = name
        self.slug = self.__class__.SLUGS.get(name) or "".join(
            c for c in name.lower() if c.isalpha()
        )

    @property
    def url(self) -> str:
        return f"https://cdn.c3voc.de/hls/{self.slug}/native_hd.m3u8"

    def at(
        self, now: t.Optional[datetime] = None
    ) -> tuple[t.Optional[Entry], t.Optional[Entry]]:
        return self.fahrplan().at(self.name, now)

    def at1(self, now: t.Optional[datetime] = None) -> t.Optional[Entry]:
        currently, next_up = self.fahrplan().at(self.name, now)
        return currently or next_up

    @property
    def menu_line(self) -> str:
        line = [self.__class__.CATEGORIES[self.name], self.name]

        now = datetime.now(tz=TZ)
        currently, next_up = self.at(now)
        if currently:
            minutes = format_duration(currently.end - now, ":play_button:")
            line.append(f"[green]{minutes}: [b]{currently.title}[/b][/green]")
        if next_up:
            minutes = format_duration(next_up.start - now, ":soon:")
            line.append(f"[yellow]{minutes}: [b]{next_up.title}[/b][/yellow]")

        return " ".join(line)

    @property
    def menu_description(self) -> t.Optional[RenderableType]:
        fpe = self.at1()
        if fpe:
            return md(fpe.description_html, html=True)
        return None

    def play(self) -> None:
        title = self.name
        description = None
        fpe = self.at1()
        if fpe:
            title = f"{title} [default yellow]{fpe.title}[/default yellow]"
            description = md(fpe.description_html, html=True)
        mpv(self.url, title, description)

    @classmethod
    def fahrplan(cls) -> Fahrplan:
        if cls._fahrplan is None:
            cls._fahrplan = Fahrplan()
        return cls._fahrplan

    @classmethod
    def fetch(cls: t.Type[_T_Stream]) -> tuple[_T_Stream, ...]:
        fahrplan = cls.fahrplan()
        fahrplan.refresh()
        rooms = set(e.room for e in fahrplan.entries)
        rooms.update(cls.NO_FAHRPLAN)
        return tuple(cls(room) for room in sorted(rooms))


class StreamMenu(Menu):
    menu_line = "Streams"
    prompt = "Select stream"

    def items(self) -> t.Sequence[Stream]:
        return Stream.fetch()
