import typing as t

from rich.console import RenderableType
from rich.panel import Panel
from rich.prompt import Prompt

from .console import console
from .run import run


def mpv(
    media: str,
    title: t.Optional[RenderableType] = None,
    description: t.Optional[RenderableType] = None,
) -> None:
    if title is None:
        title = media
    title = f"[bold green]{title}"
    console.clear()
    if description is None:
        console.rule(title)
    else:
        console.print(
            Panel(description, title=title),
            highlight=False,
        )
    if run("mpv", media):
        Prompt.ask(
            "[green]OK?[/green] [b]mpv[/b] claims it finished successfully,"
            " but you might still want to read its messages\n"
            "Hit enter when done"
        )
