# rC3 Streaming Setup

Developed for a local bubble of [rC3](https://rc3.world/). Powered
with ugly Python, [mpv](https://mpv.io/),
[fzf](https://github.com/junegunn/fzf). Originally running on
a [grml](https://grml.org/) setup.

Integrated with Fahrplan, recordings feed, and
Relive. Auto-updating. Still a bit clunky.

![Streams](screenshot-streams.png)
![Relive with search](screenshot-relive.png)

Python requirements are in `requirements.txt`, extra dev requirements in
`requirements-dev.txt`. Example mpv config is in `mpv.conf.example`. Code lives
in `./rC3` and you can run it with `python -m rC3`.

## Quick Start

1. Install Python 3 (developed on 3.10, might run on 3.9, maybe even 3.8),
   `fzf`, `mpv`
2. If you want, update mpv config (some suggestions are in the example file)
3. Run `./run.sh` - it will create a Python venv, install Python deps and start
   the menu
