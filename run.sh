#!/bin/sh
set -e

# check commands
for cmd in python3 mpv fzf ; do
  if ! which "$cmd" > /dev/null ; then
    echo "FATAL: you need to install ${cmd}" >&2
    exit 1
  fi
done

if [ "$1" = '-clean' ] ; then
  echo "# cleaning venv" >&2
  rm -rf ./venv
  shift
fi

if ! [ -x ./venv/bin/pip ] ; then
  echo "# creating venv" >&2
  python3 -m venv --upgrade-deps ./venv
fi

echo "# installing dependencies" >&2
./venv/bin/pip install -r requirements.txt

exec ./venv/bin/python3 -m rC3 "${@}"
